#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

#define zalloc(size)    calloc(size, 1)

#define dbg(format, arg...)                                                 \
     do {                                                                   \
     if (debug)                                                             \
          fprintf(stdout, "weather: %s: " format , __func__, ## arg);       \
     } while (0)

int debug;

const char owm_host[]    = "http://api.openweathermap.org/data/2.5/weather?id=";
const char owm_city_id[] = "703448"; 
const char owm_units[]   = "units=metric";
const char owm_mode[]    = "mode=xml";
const char owm_token[]   = "appid=86c27e44e7592e9663c24533efae5e92";

static CURL *curl_init(void)
{
     CURL *curl;

     curl = curl_easy_init();
     if (!curl) {
          fprintf(stderr, "Can not init CURL!\n");
          return NULL;
     }

     return curl;
}

static int send_request(void)
{
     const int req_url_size = 2000;
     char req_url[req_url_size];
     CURL *curl = NULL;
     CURLcode res;
     
     curl = curl_init();
     snprintf(req_url, req_url_size, "%s%s&%s&%s&%s",
              owm_host,
              owm_city_id,
              owm_units,
              owm_mode,
              owm_token
          );
     dbg("URL: %s\n", req_url);
     curl_easy_setopt(curl, CURLOPT_URL, req_url);
     res = curl_easy_perform(curl);

     return 0;
}

int main(int argc, char *argv[])
{
     debug = 0;
     send_request();
     return 0;
}

     

